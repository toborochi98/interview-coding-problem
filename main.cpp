#include <iostream>
#include <math.h>

# define M_PI   3.14159265358979323846

using namespace std;

const double toRad = M_PI/180;

class Rover{

    // Positions, direction, land bounds and flag
    public:
    int x,y,direction;
    int boundX,boundY;
    bool next;

    // Constructor
    Rover(int x,int y,char direction, int bx,int by){
        this->x=x;
        this->y=y;
        this->boundX=bx;
        this->boundY=by;
        this->direction = getDirectionValue(direction);
    }

    int getDirectionValue(char direction){
        return (direction=='N')?90:(direction=='S')?270:(direction=='E')?0:180;
    }

    char getDirectionChar(int angle){
        return (angle==90)?'N':(angle==270)?'S':(angle==0)?'E':'W';
    }

    // Process a single instruction
    /*
        L = Rotate Left 90`
        R = Rotate Right 90`
        M = Move forward (depending on current direction)
    */
    bool processInstruction(char c){
        switch(c){
            case 'L' :
                direction+=90; direction%=360;
                return true;
                break;
            case 'R' :
                direction = (direction==0)?270:direction-90;
                return true;
                break;
            case 'M' :
                x+= int(cos( double(direction)*toRad ));
                y+= int(sin( double(direction)*toRad ));

                if(x<=boundX && x>=0 && y>=0 && y<=boundY){
                    return true;
                }else{
                    return false;
                }
                break;
        }
    }

    // Processing a string of instructions
    void processTape(string s){
        next = true;
        for(int i=0;i<s.size() && next;++i){
            next = processInstruction(s[i]);
        }
        if(!next){
            cerr<<"ERROR: Out of bounds"<<endl;
        }
    }

    // Get current status of the Rover
    void status(int&currentX,int&currentY,char& currentF){
        currentX = x;
        currentY = y;
        currentF = getDirectionChar(direction);
    }
};

const char filename_in[] = "in.txt";
const char filename_ou[] = "ou.txt";

int main(int argc, char* argv[])
{
    // File input
    freopen(argv[1],"r",stdin);
    freopen(filename_ou,"w",stdout);

    // std input
    int n,m,initialX,initialY,a,b;
    char direction,c;
    string s;

    // Bounds(X,Y)
    cin>>n>>m;

    // Read until EOF
    while(!cin.eof()){

        cin>>initialX>>initialY>>direction>>s;

        Rover r = Rover(initialX,initialY,direction,n,m);

        r.processTape(s);

        r.status(a,b,c);

        cout<<a<<" "<<b<<" "<<c<<endl;
    }

    fclose(stdout);
    return 0;
}
