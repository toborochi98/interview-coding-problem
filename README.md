# Interview Coding Problem

Interview Problem based on: [``PDF``](https://bitbucket.org/toborochi98/interview-coding-problem/raw/535825296d5ae977f56522bd93dabd6f048d65a0/MarsRoverCodingProblem.pdf)

 *	Main Program: ``main.cpp``
 *	Compiled Program: ``bin\Debug\MarsRover.exe``

You can execute the ``MarsRover.exe`` with the following command: ``MarsRover <filename>``. After that, it generates a ``ou.txt`` file with the answers to each test case.

